const config = {
    padrao: {
        toolbar: [['Bold', 'Italic']],
    },

    padraoBr: {
        toolbar: [['Bold', 'Italic']],
        enterMode: CKEDITOR.ENTER_BR,
    },

    clean: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
    },

    cleanBr: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR,
    },

    completo: {
        toolbar: [['Bold', 'Italic'], ['Link', 'Unlink']],
        height: 400,
    },

    apresentacao: {
        format_tags: 'h1;p',
        toolbar: [['Format'], ['Bold', 'Italic']],
        height: 400,
    },

    conselho: {
        toolbar: [['Link', 'Unlink']],
        enterMode: CKEDITOR.ENTER_BR,
    },

    infoContato: {
        toolbar: [['Bold', 'Italic'], ['Link', 'Unlink']],
        enterMode: CKEDITOR.ENTER_BR,
    },
};

export default function TextEditor() {
    CKEDITOR.config.language = 'pt-br';
    CKEDITOR.config.uiColor = '#dce4ec';
    CKEDITOR.config.contentsCss = [
        `${$('base').attr('href')}/assets/ckeditor.css`,
        CKEDITOR.config.contentsCss,
    ];
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.resize_enabled = false;
    CKEDITOR.plugins.addExternal(
        'injectimage',
        `${$('base').attr('href')}/assets/injectimage/plugin.js`,
    );
    // CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraPlugins = 'injectimage';

    $('.ckeditor').each((i, obj) => {
        CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
    });
}
