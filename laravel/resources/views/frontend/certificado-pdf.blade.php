<html>
<head>
<style>
    @font-face {
        font-family: 'Lato';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/lato/v16/S6uyw4BMUTPHjx4wWw.ttf) format('truetype');
    }
    @font-face {
        font-family: 'Lato';
        font-style: normal;
        font-weight: bold;
        src: url(https://fonts.gstatic.com/s/lato/v16/S6u9w4BMUTPHh6UVSwiPHA.ttf) format('truetype');
    }

    @page { margin: 0in; }

    body {
        font-family: 'Lato', serif;
        padding: 0;
        width: 100%;
        height: 100%;
        color: #70818F;
    }
    .bg {
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        width: 842pt;
        height: 595pt;
    }
    h2 {
        font-size: 19pt;
        padding-top: 155pt;
        margin-bottom: 10pt;
        text-align: center;
    }
    p {
        padding-left: 100pt;
        padding-right: 100pt;
        font-size: 16pt;
        text-align: center;
    }
    .nome {
        font-size: 24pt;
        text-transform: uppercase;
    }
    p strong {
        text-transform: uppercase;
    }
    .carga {
        font-size: 11pt;
        text-align: center;
        font-weight: bold;
    }
    .data {
        font-size: 9pt;
        text-align: center;
    }
</style>
</head>
<body>
    <img src="assets/img/certificados/{{ $aula->certificado }}" class="bg" alt="">
    <h2>
        Certificado de Conclusão de Módulo do<br>
        PAAF - Programa de Atualização Accord Farmacêutica
    </h2>
    <p>
        Certificamos que<br>
        <span class="nome">{{ $user->nome }}</span><br>
        concluiu o módulo {{ Tools::numToRoman($aula->modulo) }} do PAAF
        pela palestra online<br>
        <strong>{{ $aula->titulo }}</strong>,
        ministrada por <strong>{{ $aula->palestrante->nome }}</strong>,
        por meio da plataforma Accord Academy.
    </p>
    <p class="carga">
        Carga Horária: {{ $aula->carga_horaria }}
    </p>
    <p class="data">
        {{ Tools::dataCertificado() }}
    </p>
</body>
</html>
