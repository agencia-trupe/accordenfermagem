@extends('frontend.common.template')

@section('content')

    <div class="main login">
        <div class="center">
            <div class="wrapper">
                <p>ESQUECI MINHA SENHA</p>

                @if(session('enviado'))
                    <div class="sucesso">
                        {{ session('enviado') }}
                    </div>
                @else
                    <form action="{{ route('redefinicao') }}" class="form-padrao form-cadastro" method="POST">
                        @if($errors->any())
                            <div class="erro">
                                @foreach($errors->all() as $error)
                                {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif

                        {!! csrf_field() !!}

                        <div class="row">
                            <label for="email">E-MAIL</label>
                            <input type="email" name="email" id="email" value="{{ old('email') }}" required>
                        </div>

                        <input type="submit" value="REDEFINIR SENHA">
                    </form>
                @endif
            </div>
        </div>
    </div>

@endsection
