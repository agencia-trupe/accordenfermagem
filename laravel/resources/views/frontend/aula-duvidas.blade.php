@extends('frontend.common.template')

@section('content')

    <div class="main texto aulas">
        <div class="center">
            <h1>AULAS</h1>

            <div class="aula">
                <div class="wrapper">
                    <div class="aula-descricao" style="margin-top:0">
                        <div class="aula-texto" style="padding-top:0">
                            <h2>{{ $aula->titulo }}</h2>
                            <h3>{{ $aula->palestrante->nome }}</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="aula-forum">
                <h3>FÓRUM</h3>
                <p>Envie suas dúvidas para o especialista.</p>
                <form action="" id="form-duvida" data-url="{{ route('aulas.duvidaPost', $aula->id) }}">
                    <textarea name="duvida" required></textarea>
                    <input type="submit" value="ENVIAR">
                    <a href="{{ route('aulas.show', $aula->slug) }}">voltar</a>
                    <div id="form-duvida-response"></div>
                </form>
                <div class="duvidas">
                    @foreach($duvidas as $duvida)
                    <div class="duvida">
                        <span class="dados">
                            {{ $duvida->cadastro->nome }} &middot;
                            {{ $duvida->created_at->format('d/m/Y | H:i\h') }}
                        </span>
                        <p>{!! nl2br($duvida->duvida) !!}</p>
                        <p class="resposta">{!! nl2br($duvida->resposta) !!}</p>
                    </div>
                    @endforeach

                    {!! $duvidas->render() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
