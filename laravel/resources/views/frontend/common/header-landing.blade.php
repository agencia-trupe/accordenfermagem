    @if(!Tools::routeIs('home'))
    <div class="barra-perfil">
        <div class="center">
            PERFIL ENFERMAGEM
        </div>
    </div>
    @endif

    <header class="header-landing @if(Tools::routeIs('home')) header-fixed @endif">
        <div class="center">
            <div class="wrapper">
                <a href="{{ env('MAIN_URL') }}" class="logo">
                    <img src="{{ asset('assets/img/layout/marca-accordacademy-abrenfoh.png') }}" alt="{{ config('app.name') }}">
                </a>
                <nav>
                    <a href="{{ env('MAIN_URL') }}#sobre">SOBRE</a>
                    <a href="{{ env('MAIN_URL') }}#contato">CONTATO</a>
                    <a href="{{ env('MAIN_URL') }}#login">ENTRE OU CADASTRE-SE</a>
                </nav>
            </div>
        </div>
    </header>
