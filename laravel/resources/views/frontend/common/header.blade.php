    @if(auth('cadastro')->check())
    <div class="barra-cadastro">
        <div class="center">
            <span>
                Olá
                {{ explode(' ', auth('cadastro')->user()->nome)[0] }}!
            </span>
            <a href="{{ route('dados') }}" @if(Tools::routeIs('dados')) active @endif>
                EDITAR CADASTRO
            </a>
            <a href="{{ route('logoutCadastro') }}">
                SAIR
            </a>
        </div>
    </div>
    @endif

    <div class="barra-perfil">
        <div class="center">
            PERFIL ENFERMAGEM
        </div>
    </div>

    <header>
        <div class="center">
            <div class="wrapper">
                <a href="{{ env('MAIN_URL') }}" class="logo">
                    <img src="{{ asset('assets/img/layout/marca-accordacademy-abrenfoh.png') }}" alt="{{ config('app.name') }}">
                </a>

                <nav>
                    <a href="{{ route('sobre') }}" @if(Tools::routeIs('sobre')) class="active" @endif>
                        SOBRE
                    </a>
                    <a href="{{ route('palestrantes') }}" @if(Tools::routeIs('palestrantes')) class="active" @endif>PALESTRANTES</a>
                    <a href="{{ route('aulas') }}" @if(Tools::routeIs('aulas*')) class="active" @endif>AULAS</a>
                    <a href="{{ route('certificado') }}" @if(Tools::routeIs('certificado')) class="active" @endif>CERTIFICADO</a>
                </nav>
            </div>
        </div>
    </header>
