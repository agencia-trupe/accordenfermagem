    <div class="sobrafo">
        <img src="{{ asset('assets/img/layout/accord-abrenfoh.png') }}" alt="">
    </div>
    <footer>
        <div class="center">
            <div class="wrapper">
                <img src="{{ asset('assets/img/layout/marca-accord-rodape.png') }}" alt="">

                <div class="informacoes">
                    <p>
                        Informações de contato
                        <span>{{ $contato->telefone }}</span>
                        <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                    </p>
                </div>

                <div class="termos">
                    <p>{!! $contato->termos_rodape !!}</p>
                </div>
            </div>

            <div class="copyright">
                <p>
                    &copy; {{ config('app.name') }} - Todos os direitos reservados.
                </p>
            </div>
        </div>
    </footer>
