@extends('frontend.common.template')

@section('content')

    <div class="main texto aulas">
        <div class="center">
            <h1>AULAS</h1>

            @if($aula->video)
            <div class="video-wrapper" data-concluir="{{ route('aulas.assistida', $aula->id) }}">
                <iframe src="{{ $aula->video }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen scrolling="no" style="overflow: hidden"></iframe>
            </div>
            @endif

            <div class="aula">
                <div class="wrapper">
                    <div class="aula-descricao">
                        <div class="fotos-wrapper">
                            @if($aula->moderadora_foto)
                            <div class="palestrante-foto">
                                <img src="{{ asset('assets/img/moderadoras/'.$aula->moderadora_foto) }}" alt="">
                            </div>
                            @endif
                            <div class="palestrante-foto">
                                <img src="{{ asset('assets/img/palestrantes/'.$aula->palestrante->foto) }}" alt="">
                            </div>
                        </div>
                        <div class="aula-texto">
                            <h2>{{ $aula->titulo }}</h2>
                            <p>{!! $aula->descricao !!}</p>
                            <h3 style="line-height: 1.5">
                                @if($aula->moderadora)
                                Moderadora: {{ $aula->moderadora }}<br>
                                @endif
                                Palestrante: {{ $aula->palestrante->nome }}
                            </h3>
                        </div>
                    </div>

                    @if(count($aula->questoes))
                    <div class="aula-quiz">
                        <h4>TESTE SEUS CONHECIMENTOS</h4>

                        @if($aula->questionarioBloqueado)
                            <p class="aviso">Você não atingiu os requisitos mínimos para emissão do Certificado deste módulo. Por gentileza, retorne após o prazo de 72 horas para refazer o teste de conhecimento. Agradecemos a participação. Accord Academy</p>
                        @else
                            @foreach($aula->questoes as $key => $questao)
                            <?php
                                $resposta = $user->respostaQuestao($questao->id);
                                ?>
                            <div class="aula-questao @if($resposta) respondida @endif" data-route="{{ route('aulas.questaoPost', [$aula->id, $questao->id]) }}">
                                    <span>{{ $key + 1 }}</span>
                                <p>{{ $questao->questao }}</p>
                                @foreach($questao->alternativas as $alternativa)
                                <label class="@if($resposta && $resposta->id == $alternativa->id) selecao @if($resposta && $alternativa->alternativa_correta) correta @endif @endif">
                                        <input type="radio" name="alternativa-{{$questao->id}}" value="{{ $alternativa->id }}" @if($resposta) disabled @endif>
                                        <span class="custom-radio"></span>
                                        {{ $alternativa->alternativa }}
                                </label>
                                @endforeach
                                @if(!$resposta)
                                <button>RESPONDER</button>
                                @elseif($user->questaoCerta($questao->id))
                                <button class="certo">VOCÊ ACERTOU</button>
                                @else
                                <button class="errado">VOCÊ ERROU</button>
                                @endif
                            </div>
                            @endforeach
                        @endif
                    </div>
                    @endif
                </div>

                <div class="aula-forum">
                    <h3>FÓRUM</h3>
                    <p>Envie suas dúvidas para o especialista.</p>
                    <form action="" id="form-duvida" data-url="{{ route('aulas.duvidaPost', $aula->id) }}">
                        <textarea name="duvida" required></textarea>
                        <input type="submit" value="ENVIAR">
                        <div id="form-duvida-response"></div>
                    </form>
                    @if(count($duvidas))
                    <div class="duvidas">
                        @foreach($duvidas->take(6) as $duvida)
                        <div class="duvida">
                            <span class="dados">
                                {{ $duvida->cadastro->nome }} &middot;
                                {{ $duvida->created_at->format('d/m/Y | H:i\h') }}
                            </span>
                            <p>{!! nl2br($duvida->duvida) !!}</p>
                            <p class="resposta">{!! nl2br($duvida->resposta) !!}</p>
                        </div>
                        @endforeach

                        @if(count($duvidas) > 6)
                        <a href="{{ route('aulas.duvidas', $aula->slug) }}">
                            <span>VER MAIS</span>
                        </a>
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
