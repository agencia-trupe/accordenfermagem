@extends('frontend.common.template')

@section('content')

    <div class="main login">
        <div class="center">
            <div class="wrapper">
                <h2>ENTRE OU CADASTRE-SE</h2>
                <h3>PRIMEIRO CADASTRO - ENFERMAGEM</h3>

                <form action="{{ route('cadastroPost') }}" class="form-padrao form-cadastro" method="POST">
                    @if($errors->any())
                        <div class="erro">
                            @foreach($errors->all() as $error)
                            {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

                    {!! csrf_field() !!}

                    <div class="row">
                        <label for="nome">NOME COMPLETO</label>
                        <input type="text" name="nome" id="nome" value="{{ old('nome') }}" required>
                    </div>
                    <div class="row">
                        <label for="email">E-MAIL</label>
                        <input type="email" name="email" id="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="row">
                        <label for="registro_funcional_tipo" class="lg">TIPO DE REGISTRO FUNCIONAL</label>
                        <input type="text" name="registro_funcional_tipo" id="registro_funcional_tipo" value="{{ old('registro_funcional_tipo') }}" required>
                    </div>
                    <div class="row">
                        <label for="registro_funcional_uf" class="lg">UF DO NÚMERO DE REGISTRO FUNCIONAL</label>
                        <select name="registro_funcional_uf" id="registro_funcional_uf" required>
                            <option value="">Selecione</option>
                            @foreach(Tools::listaUf() as $uf)
                            <option value="{{ $uf }}" @if(old('registro_funcional_uf') == $uf) selected @endif>{{ $uf }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <label for="registro_funcional">REGISTRO FUNCIONAL</label>
                        <input type="text" name="registro_funcional" id="registro_funcional" value="{{ old('registro_funcional') }}" required>
                    </div>
                    <div class="row">
                        <label for="especialidade">ESPECIALIDADE</label>
                        <input type="text" name="especialidade" id="especialidade" value="{{ old('especialidade') }}" required>
                    </div>
                    <div class="row">
                        <label for="cargo">CARGO</label>
                        <input type="text" name="cargo" id="cargo" value="{{ old('cargo') }}" required>
                    </div>
                    <div class="row">
                        <label for="empresa">EMPRESA</label>
                        <input type="text" name="empresa" id="empresa" value="{{ old('empresa') }}" required>
                    </div>
                    <div class="row">
                        <label for="senha">SENHA</label>
                        <input type="password" name="senha" id="senha" required>
                    </div>
                    <div class="row">
                        <label for="senha_confirmation">REPETIR SENHA</label>
                        <input type="password" name="senha_confirmation" id="senha_confirmation" required>
                    </div>

                    <input type="submit" class="submit-lg" value="CADASTRAR">
                </form>
            </div>
        </div>
    </div>

@endsection
