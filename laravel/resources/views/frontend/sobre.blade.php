@extends('frontend.common.template')

@section('content')

    <div class="main sobre">
        <div class="center">
            <div class="texto">
                <h1>{!! $sobre->titulo !!}</h1>
                {!! $sobre->texto !!}
            </div>
        </div>
    </div>

@endsection
