@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / {{ $aula->titulo }} /</small> Editar Questão</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.questoes.update', $aula->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aulas.questoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
