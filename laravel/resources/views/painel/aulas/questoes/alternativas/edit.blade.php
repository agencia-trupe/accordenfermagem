@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / {{ $aula->titulo }} / Questão /</small> Editar Alternativa</h2>
    </legend>

    <div class="alert alert-info">{{ $questao->questao }}</div>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.questoes.alternativas.update', $aula->id, $questao->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aulas.questoes.alternativas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
