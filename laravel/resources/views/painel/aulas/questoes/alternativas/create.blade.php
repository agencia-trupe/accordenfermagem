@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / {{ $aula->titulo }} / Questão /</small> Adicionar Alternativa</h2>
    </legend>

    <div class="alert alert-info">{{ $questao->questao }}</div>

    {!! Form::open(['route' => ['painel.aulas.questoes.alternativas.store', $aula->id, $questao->id]]) !!}

        @include('painel.aulas.questoes.alternativas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
