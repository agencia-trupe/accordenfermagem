@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.aulas.questoes.index', $aula->id) }}" title="Voltar para Questões" class="btn btn-sm btn-default">
        &larr; Voltar para Questões
    </a>

    <legend>
        <h2>
            <small>Aulas / {{ $aula->titulo }} / Questão /</small> Alternativas
            <a href="{{ route('painel.aulas.questoes.alternativas.create', [$aula->id, $questao->id]) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Alternativa</a>
        </h2>
    </legend>

    <div class="alert alert-info">{{ $questao->questao }}</div>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="aulas_questoes_alternativas">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Alternativa</th>
                <th>Alternativa Correta</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->alternativa }}</td>
                <td>
                    @if($registro->alternativa_correta)
                    <span class="glyphicon glyphicon-ok"></span>
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.aulas.questoes.alternativas.destroy', $aula->id, $questao->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.aulas.questoes.alternativas.edit', [$aula->id, $questao->id, $registro->id]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
