@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.aulas.index') }}" title="Voltar para Aulas" class="btn btn-sm btn-default">
        &larr; Voltar para Aulas
    </a>

    <legend>
        <h2>
            <small>Aulas / {{ $aula->titulo }} /</small> Dúvidas
        </h2>
    </legend>

    <div class="btn-group">
        <a href="{{ route('painel.aulas.duvidas.index', [$aula->id, 'filtro' => 'em-aberto']) }}" class="btn btn-info @if(request('filtro') != 'respondidas') active @endif">Em aberto</a>
        <a href="{{ route('painel.aulas.duvidas.index', [$aula->id, 'filtro' => 'respondidas']) }}" class="btn btn-info @if(request('filtro') == 'respondidas') active @endif">Respondidas</a>
    </div>

    <hr>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Usuário</th>
                <th>Dúvida</th>
                <th>Enviada em</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->cadastro->nome }}</td>
                <td>
                    {{ $registro->duvida }}
                    @if($registro->resposta)
                    <hr style="margin:8px 0">
                    <small><strong>{{ $registro->resposta }}</strong></small>
                    @endif
                </td>
                <td style="white-space: nowrap">{{ $registro->created_at->format('d/m/Y | H:i\h') }}</td>
                <td class="crud-actions" style="width:215px">
                    {!! Form::open([
                        'route'  => ['painel.aulas.duvidas.destroy', $aula->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.aulas.duvidas.edit', [$aula->id, $registro->id]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>
                            {{ $registro->resposta ? 'Editar' : 'Responder' }}
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->appends($_GET)->render() !!}
    @endif

@endsection
