@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('cadastro', 'Usuário') !!}
            {!! Form::text('cadastro', $registro->cadastro->nome, ['class' => 'form-control', 'disabled']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('enviada_em', 'Enviada em') !!}
            {!! Form::text('enviada_em', $registro->created_at->format('d/m/Y | H:i\h'), ['class' => 'form-control', 'disabled']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('duvida', 'Dúvida') !!}
    {!! Form::textarea('duvida', null, ['class' => 'form-control', 'style' => 'resize:none;height:150px']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta', 'Resposta') !!}
    {!! Form::textarea('resposta', null, ['class' => 'form-control', 'style' => 'resize:none;height:150px']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aulas.duvidas.index', [$aula->id, 'filtro' => ($registro->resposta ? 'respondidas' : 'em-aberto')]) }}" class="btn btn-default btn-voltar">Voltar</a>
