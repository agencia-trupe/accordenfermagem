@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Aulas
            <a href="{{ route('painel.aulas.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Aula</a>
        </h2>
    </legend>

    <form action="{{ route('painel.aulas.aviso') }}" method="POST">
        {!! csrf_field() !!}
        {{ method_field('PUT') }}

        <div class="form-group">
            {!! Form::label('aviso', 'Aviso') !!}
            <div style="display:flex">
                {!! Form::text('aviso', $aviso, ['class' => 'form-control']) !!}
                {!! Form::submit('Salvar', ['class' => 'btn btn-success', 'style' => 'margin-left: 10px']) !!}
            </div>
        </div>
    </form>

    <hr>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="aulas">
        <thead>
            <tr>
                <th>Data de liberação</th>
                <th>Módulo</th>
                <th>Palestrante</th>
                <th>Título</th>
                <th>Questões</th>
                <th>Dúvidas</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    @if($registro->isAtiva)
                    <span class="glyphicon glyphicon-ok text-success" style="margin-right:5px"></span>
                    @endif
                    {{ $registro->data_liberacao }}
                </td>
                <td>{{ $registro->modulo }}</td>
                <td>
                    {{ $registro->palestrante->nome }}
                    @if($registro->moderadora)
                    <small style="display:block">Moderadora: {{ $registro->moderadora }}</small>
                    @endif
                </td>
                <td>{{ $registro->titulo }}</td>
                <td>
                    <a href="{{ route('painel.aulas.questoes.index', $registro->id) }}" class="btn btn-info btn-sm">
                        <span class="glyphicon glyphicon-th-list" style="margin-right:10px;"></span>Gerenciar
                    </a>
                </td>
                <td>
                    <a href="{{ route('painel.aulas.duvidas.index', $registro->id) }}" class="btn btn-success btn-sm">
                        <span class="glyphicon glyphicon-question-sign" style="margin-right:10px;"></span>
                        Gerenciar
                        <span class="label label-info" style="margin-left:3px">{{ $registro->duvidas()->emAberto()->count() }}</span>
                    </a>
                </td>
                <td class="crud-actions" style="width:265px">
                    {!! Form::open([
                        'route'  => ['painel.aulas.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.aulas.previa', $registro->id) }}" target="_blank" class="btn btn-warning btn-sm pull-left">
                            <span class="glyphicon glyphicon-eye-open" style="margin-right:10px"></span>Prévia
                        </a>
                        <a href="{{ route('painel.aulas.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
