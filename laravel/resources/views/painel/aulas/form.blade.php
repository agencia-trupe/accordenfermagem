@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data_liberacao', 'Data de liberação') !!}
    {!! Form::text('data_liberacao', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('palestrante_id', 'Palestrante') !!}
    {!! Form::select('palestrante_id', $palestrantes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('moderadora', 'Moderadora') !!}
            {!! Form::text('moderadora', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('moderadora_foto', 'Moderadora - Foto') !!}
        @if($submitText == 'Alterar' && $registro->moderadora_foto)
            <img src="{{ url('assets/img/moderadoras/'.$registro->moderadora_foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%; border-radius: 100%">
        @endif
            {!! Form::file('moderadora_foto', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('modulo', 'Número do módulo') !!}
    {!! Form::number('modulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('carga_horaria', 'Carga horária') !!}
    {!! Form::text('carga_horaria', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição (opcional)') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/aulas/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Código do Vídeo') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('certificado', 'Modelo de certificado') !!}
@if($submitText == 'Alterar' && $registro->certificado)
    <div style="margin-bottom:5px">
        <a href="{{ route('painel.aulas.certificado', $registro->id) }}" class="btn btn-info">Testar geração do certificado</a>
    </div>
    <img src="{{ url('assets/img/certificados/'.$registro->certificado) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('certificado', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aulas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
