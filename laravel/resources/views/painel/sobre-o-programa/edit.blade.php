@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Sobre o Programa</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.sobre-o-programa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.sobre-o-programa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
