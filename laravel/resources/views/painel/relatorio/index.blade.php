@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Relatório
        </h2>
    </legend>


    @if(!count($aulas))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="palestrantes">
        <thead>
            <tr>
                <th>Aula</th>
                <th>Usuários aptos ao certificado</th>
            </tr>
        </thead>

        <tbody>
        @foreach ($aulas as $aula)
            <tr>
                <td>
                    <a href="{{ route('painel.relatorio.show', $aula->id) }}">
                        {{ $aula->titulo }}
                    </a>
                </td>
                <td>{{ count($aula->aptos) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
