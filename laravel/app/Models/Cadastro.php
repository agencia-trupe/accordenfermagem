<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Cadastro extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'cadastros';

    protected $guarded = ['id'];

    protected $hidden = ['senha', 'remember_token'];

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public function aulas()
    {
        $questoesRespondidas = $this->respostas->lists('questao_id')->toArray();

        return Aula::whereHas('questoes', function($q) use ($questoesRespondidas) {
           $q->whereIn('id', $questoesRespondidas);
        });
    }

    public function getAulasAttribute()
    {
        return $this->aulas()->get();
    }

    public function getAulasConcluidasAttribute()
    {
        return $this->aulas->unique();
    }

    public function getFracaoAulasConcluidasAttribute()
    {
        return $this->aulasConcluidas->count().' / '. Aula::cachedCount();
    }

    public function getPorcentagemAulasConcluidasAttribute()
    {
        try {
            return round(($this->aulasConcluidas->count() / Aula::cachedCount()) * 100);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function respostas()
    {
        return $this->hasMany(CadastroQuestaoResposta::class, 'cadastro_id');
    }

    public function respostaQuestao($id)
    {
        $resposta = $this->respostas()->questao($id)->first();

        return $resposta ? $resposta->alternativa : null;
    }

    public function questaoCerta($id)
    {
        return !!$this->respostas()->with('alternativa')->questao($id)
            ->whereHas('alternativa', function($q) {
                $q->where('alternativa_correta', true);
            })->count();
    }

    public function todasAulasConcluidas()
    {
        return $this->porcentagemAulasConcluidas == 100;
    }

    public function acertosPorAula($id)
    {
        $aula = Aula::find($id);

        if (! $aula) return 0;

        return $this->respostas()
            ->whereHas('questao', function($q) use ($aula) {
                $q->where('aula_id', $aula->id);
            })->whereHas('alternativa', function($q) {
                $q->where('alternativa_correta', true);
            })->count();
    }

    public function porcentagemDeAcertos()
    {
        $respostas = $this->respostas();
        $aulas     = Aula::with('questoes')->get();

        $acertos = [];
        foreach($aulas as $aula) {
            try {
                $acertos[$aula->id] = round(($this->acertosPorAula($aula->id) / $aula->questoes->count()) * 100);
            } catch(\Exception $e) {
                $acertos[$aula->id] = 0;
            }
        }

        return $acertos;
    }

    public function mediaValidaDeAcertos()
    {
        return min($this->porcentagemDeAcertos()) >= 70;
    }

    public function aptoAoCertificado()
    {
        return $this->todasAulasConcluidas() && $this->mediaValidaDeAcertos();
    }
}
