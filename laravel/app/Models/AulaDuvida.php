<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AulaDuvida extends Model
{
    protected $table = 'aulas_duvidas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('id', 'DESC');
    }

    public function scopeEmAberto($query)
    {
        return $query->where('resposta', '');
    }

    public function scopeRespondidas($query)
    {
        return $query->where('resposta', '<>', '');
    }

    public function cadastro()
    {
        return $this->belongsTo(Cadastro::class, 'cadastro_id');
    }

    public function aula()
    {
        return $this->belongsTo(Aula::class, 'aula_id');
    }
}
