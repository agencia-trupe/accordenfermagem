<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CadastrosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nome'                    => 'required',
            'email'                   => 'required|email|unique:cadastros,email',
            'registro_funcional_tipo' => 'required',
            'registro_funcional_uf'   => 'required',
            'registro_funcional'      => 'required',
            'especialidade'           => 'required',
            'cargo'                   => 'required',
            'empresa'                 => 'required',
            'senha'                   => 'required|confirmed|min:6',
        ];

        if ($this->method() != 'POST') {
            $rules['email'] = 'required|email|unique:cadastros,email,'.auth('cadastro')->user()->id;
            $rules['senha'] = 'confirmed|min:6';
        }

        return $rules;
    }

    public function messages() {
        return [
            'nome.required'                    => 'preencha seu nome',
            'email.required'                   => 'insira um endereço de e-mail válido',
            'email.email'                      => 'insira um endereço de e-mail válido',
            'email.unique'                     => 'o e-mail inserido já está cadastrado',
            'registro_funcional_tipo.required' => 'preencha seu tipo de registro funcional',
            'registro_funcional_uf.required'   => 'preencha seu UF do número de registro funcional',
            'registro_funcional.required'      => 'preencha seu registro funcional',
            'especialidade.required'           => 'preencha sua especialidade',
            'cargo.required'                   => 'preencha seu cargo',
            'empresa.required'                 => 'preencha sua empresa',
            'senha.required'                   => 'insira uma senha',
            'senha.confirmed'                  => 'a confirmação de senha não confere',
            'senha.min'                        => 'sua senha deve ter no mínimo 6 caracteres',
        ];
    }
}
