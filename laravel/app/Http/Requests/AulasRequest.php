<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AulasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data_liberacao' => 'required',
            'palestrante_id' => 'required',
            'moderadora' => '',
            'moderadora_foto' => 'image',
            'titulo' => 'required',
            'modulo' => 'required|integer|min:1',
            'carga_horaria' => 'required',
            'descricao' => '',
            'capa' => 'required|image',
            'certificado' => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'palestrante_id' => 'palestrante'
        ];
    }
}
