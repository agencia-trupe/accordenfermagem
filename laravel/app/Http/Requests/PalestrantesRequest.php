<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PalestrantesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'apresentacao' => 'required',
            'foto' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }
}
