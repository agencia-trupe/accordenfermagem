<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Aula;
use App\Models\Cadastro;

class RelatorioController extends Controller
{
    public function index()
    {
        $users = Cadastro::with(['respostas.alternativa', 'respostas.questao.aula'])->get();
        $aulas = Aula::with('questoes')
            ->has('questoes')
            ->ordenados()
            ->get();

        $aulas = $aulas->map(function($aula) use ($users) {
            $totalQuestoes = $aula->questoes->count();

            $aula->aptos = $users->filter(function($user) use ($totalQuestoes, $aula) {
                $respostas = $user->respostas->filter(function($resposta) use ($aula) {
                    return $resposta->questao->aula->id === $aula->id;
                });

                $totalRespondido = count($respostas);
                $respostasCorretas = $respostas->filter(function($resposta) {
                    return $resposta->alternativa->alternativa_correta;
                })->count();

                $respondida = $totalRespondido == $totalQuestoes;
                $porcentagemDeAcertos = round(($respostasCorretas / $totalQuestoes) * 100);

                return $respondida && $porcentagemDeAcertos >= 75;
            });

            return $aula;
        });

        return view('painel.relatorio.index', compact('aulas'));
    }

    public function show(Aula $aula)
    {
        $totalQuestoes = $aula->questoes->count();

        $users = Cadastro::orderBy('nome')->get()->filter(function($user) use ($totalQuestoes, $aula) {
            $respostas = $user->respostas->filter(function($resposta) use ($aula) {
                return $resposta->questao->aula->id === $aula->id;
            });

            $totalRespondido = count($respostas);
            $respostasCorretas = $respostas->filter(function($resposta) {
                return $resposta->alternativa->alternativa_correta;
            })->count();

            $respondida = $totalRespondido == $totalQuestoes;
            $porcentagemDeAcertos = round(($respostasCorretas / $totalQuestoes) * 100);

            return $respondida && $porcentagemDeAcertos >= 75;
        });

        return view('painel.relatorio.show', compact('aula', 'users'));
    }
}
