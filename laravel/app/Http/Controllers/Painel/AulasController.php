<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AulasRequest;
use App\Http\Controllers\Controller;

use App\Models\Aula;
use App\Models\AulasAviso;
use App\Models\Palestrante;

class AulasController extends Controller
{
    public function index()
    {
        $registros = Aula::ordenados()->get();
        $aviso = AulasAviso::first()->aviso;

        return view('painel.aulas.index', compact('registros', 'aviso'));
    }

    public function updateAviso(Request $request)
    {
        $aviso = AulasAviso::first();

        $aviso->aviso = $request->aviso;
        $aviso->save();

        return redirect()->route('painel.aulas.index')->with('success', 'Aviso salvo com sucesso.');
    }

    public function create()
    {
        $palestrantes = Palestrante::ordenados()->lists('nome', 'id');

        return view('painel.aulas.create', compact('palestrantes'));
    }

    public function store(AulasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Aula::upload_capa();
            if (isset($input['certificado'])) $input['certificado'] = Aula::upload_certificado();
            if (isset($input['moderadora_foto'])) $input['moderadora_foto'] = Aula::upload_moderadora_foto();

            Aula::create($input);

            return redirect()->route('painel.aulas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Aula $registro)
    {
        $palestrantes = Palestrante::ordenados()->lists('nome', 'id');

        return view('painel.aulas.edit', compact('registro', 'palestrantes'));
    }

    public function update(AulasRequest $request, Aula $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Aula::upload_capa();
            if (isset($input['certificado'])) $input['certificado'] = Aula::upload_certificado();
            if (isset($input['moderadora_foto'])) $input['moderadora_foto'] = Aula::upload_moderadora_foto();

            $registro->update($input);

            return redirect()->route('painel.aulas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aula $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.aulas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function certificado(Aula $aula)
    {
        $user = new \stdClass();
        $user->nome = 'Teste de Certificado';

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml(view(
            'frontend.certificado-pdf', compact('user', 'aula')
        )->render());
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("AccordAcademy_Certificado-Modulo{$aula->modulo}.pdf");
    }

    public function previa(Aula $aula)
    {
        $duvidas = [];
        $user    = new \App\Models\Cadastro;

        echo '<h1 style="text-align:center;color:red;background:#fff;margin:0;padding:1em;">PRÉVIA DA VISUALIZAÇÃO DA AULA</h1>';

        return view('frontend.aula', compact('aula', 'duvidas', 'user'));
    }
}
