<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PalestrantesRequest;
use App\Http\Controllers\Controller;

use App\Models\Palestrante;

class PalestrantesController extends Controller
{
    public function index()
    {
        $registros = Palestrante::ordenados()->get();

        return view('painel.palestrantes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.palestrantes.create');
    }

    public function store(PalestrantesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = Palestrante::upload_foto();

            Palestrante::create($input);

            return redirect()->route('painel.palestrantes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Palestrante $registro)
    {
        return view('painel.palestrantes.edit', compact('registro'));
    }

    public function update(PalestrantesRequest $request, Palestrante $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = Palestrante::upload_foto();

            $registro->update($input);

            return redirect()->route('painel.palestrantes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Palestrante $registro)
    {
        try {

            if ($registro->aulas->count()) {
                throw new \Exception('Não é possível remover um palestrante que possui aulas cadastradas em seu nome.', 1);
            }

            $registro->delete();

            return redirect()->route('painel.palestrantes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
