<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AulasDuvidasRequest;
use App\Http\Controllers\Controller;

use App\Models\Aula;
use App\Models\AulaDuvida;

class AulasDuvidasController extends Controller
{
    public function index(Request $request, Aula $aula)
    {
        if ($request->filtro == 'respondidas') {
            $registros = $aula->duvidas()->respondidas()->ordenados()->paginate(15);
        } else {
            $registros = $aula->duvidas()->emAberto()->ordenados()->paginate(15);
        }

        return view('painel.aulas.duvidas.index', compact('aula', 'registros'));
    }

    public function edit(Aula $aula, AulaDuvida $registro)
    {
        return view('painel.aulas.duvidas.edit', compact('aula', 'registro'));
    }

    public function update(AulasDuvidasRequest $request, Aula $aula, AulaDuvida $registro)
    {
        try {

            $input = $request->only(['duvida', 'resposta']);

            $registro->update($input);

            return redirect()->route('painel.aulas.duvidas.index', $aula->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aula $aula, AulaDuvida $registro)
    {
        try {

            $registro->delete();

            return back()->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
