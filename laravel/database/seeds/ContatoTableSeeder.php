<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'sac@accordfarma.com.br',
            'telefone' => '11 5678 1234',
            'termos_rodape' => 'Termos e condições de uso do site.<br>Lorem ipsum dolor sit, consectetur adipiscing elit. Pellentesque malesuada eleifend nisl, sed tristique eros sagittis quis. Donec nibh turpis, feugiat ac sapien ac, sagittis interdum lectus.',
        ]);
    }
}
