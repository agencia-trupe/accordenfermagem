<?php

use Illuminate\Database\Seeder;

class SobreOProgramaSeeder extends Seeder
{
    public function run()
    {
        DB::table('sobre_o_programa')->insert([
            'apresentacao' => 'Lorem ipsum dolor sit amet<br>consectetur adipiscing elit',
            'titulo' => 'TÍTULO E TEXTO A RESPEITO<br>DO PROGRAMA',
            'texto' => '<p>Lorem ipsum dolor sit, consectetur adipiscing elit. Pellentesque malesuada eleifend nisl, sed tristique eros sagittis quis. Donec nibh turpis, feugiat ac sapien ac, sagittis interdum lectus. Ut pretium sollicitudin risus sit amet vehicula. Nunc egestas mauris lorem, nec fringilla elit commodo sit amet. Cras ornare nibh facilisis nibh lacinia, eu pellentesque nulla auctor. Nunc in pretium nibh, facilisis faucibus arcu. In interdum nunc eu libero finibus feugiat. Quisque mollis lorem ac velit malesuada tristique eu at tellus. Donec purus elit, sollicitudin nec laoreet quis, blandit eget sem. Cras metus nunc, condimentum a volutpat non, condimentum ut justo.</p><p>Pellentesque at dolor ornare, facilisis lectus ut, pretium nunc. Sed dignissim sed augue at ornare. Proin sed odio non erat ornare feugiat. Nam sollicitudin quis ligula lacinia ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean nisi mi, luctus eget est et, rhoncus molestie quam. Donec in leo eu ante volutpat posuere nec sit amet erat.</p>',
        ]);
    }
}
