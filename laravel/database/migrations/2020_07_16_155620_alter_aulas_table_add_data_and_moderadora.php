<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAulasTableAddDataAndModeradora extends Migration
{
    public function up()
    {
        Schema::table('aulas', function (Blueprint $table) {
            $table->string('data_liberacao')->after('slug');
            $table->string('moderadora')->after('palestrante_id');
            $table->string('moderadora_foto')->after('moderadora');
        });
    }

    public function down()
    {
        Schema::table('aulas', function (Blueprint $table) {
            $table->dropColumn('data_liberacao');
            $table->dropColumn('moderadora');
            $table->dropColumn('moderadora_foto');
        });
    }
}
