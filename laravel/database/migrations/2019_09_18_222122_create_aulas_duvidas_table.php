<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAulasDuvidasTable extends Migration
{
    public function up()
    {
        Schema::create('aulas_duvidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cadastro_id')->unsigned()->nullable();
            $table->foreign('cadastro_id')->references('id')->on('cadastros')->onDelete('set null');
            $table->integer('aula_id')->unsigned()->nullable();
            $table->foreign('aula_id')->references('id')->on('aulas')->onDelete('set null');
            $table->text('duvida');
            $table->text('resposta');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aulas_duvidas');
    }
}
